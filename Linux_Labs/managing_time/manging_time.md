# Managing Time

###### NOTE
 Use at least 2 machines for this lab

## Lab 1
- Configure vm as time server, using the protocol of your selection. before that, make sure that system time and hardware are synced.
Configure another node (or 2 nodes) as ntp client. monitor the sync status on the client.