# Using File Management Tools

## Lab 1
- Create a folders code,dev and ops at `/tmp/data`.
- Copy all files that start with a,b or c from `/etc/` to `/tmp/data`.
- From `/tmp/data`, move all files that have a name starting with an a or b to /tmp/data/dev and all other files /tmp/data/code.
- Find all files in `/etc` that have a size smaller than 1000 bytes and copy those to /tmp/data/ops/.
- Create symbolic link to `/var` at `/tmp/data`.

---