# Managing Processes

## Lab 1

From a root shell, start 3 process of sleep 50000 as background job.
Use the appropriate comand to show theat they are Running as expected.
Change process priority so that one of those jobs gets double the amount of CPU resource.
Terminate all 3 processes.

---

## Lab 2

- Create new project called merrie array melodies at github/gitlab/bitbuket and keep the scripts there.
- Create a script that will have Functions:
  - Function to get data and to be save them in array.
  - Function that will go throught the data and get every element with digits in it, for every iteration add a sleep command of 1 second.
  - Function that will Print out the waiting bar while the above Function (b) is Running.
  - All Functions above must Run in Function called main.
  - Main Function may Run only if root User is Running it.