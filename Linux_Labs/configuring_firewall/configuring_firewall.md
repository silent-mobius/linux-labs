# Configuring The Firewall

## Lab 1

- Use iptable to build a firewall that allows the following:
  - Incoming: ssh, ping.
  - Outgoing: ssh, dns, ping http.