# Linux Install

## Lab 1
- Install Linux distro on your laptop/desktop/machine
- Install virtualization service.
- Install RPM based distro.
- Install APT based distro.

---

## Lab 2
- Use Kickstart file generator to Create your own custom system for remote Install.
- Setup PXE system that will generate connection and will serve your kickstart file.

---