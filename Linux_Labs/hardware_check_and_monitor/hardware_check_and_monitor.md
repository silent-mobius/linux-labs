# Hardware Check and Monitor

## Lab 1

- list all block devices.
- list which of devices are mounted.
- list which of the device are mounted and how much of them is taken.
- list all the usb devices on the system.
- list all the pci devices on the system ang catch the display device.

## Lab 2
- Write a script that:
  - has a Function that lists all block devices.
  - has a Function that lists all network devices.
  - main Function that gets a parameter of User, that checks if he exists.
  - loop that will ask what to do.

##### NOTE:
`Please read about regex in the linux_fun.pdf: chapter 21 Please read about proccesses and networking in the linuxsys.pdf: chapters 1,2,3,21,22,23,24`

---

# links:

https://www.grymoire.com/
https://www.theunixschool.com/
https://www.tutorialspoint.com/awk/
https://www.tutorialspoint.com/sed/