# Managing Partitions And File Systems

## Disk partitions

### Lab 1

###### NOTE: verify that disks are mountable.

- create a script that will take a required disk name and will delete all current paritions.
- create a script that will take a required disk name and will create 2 primary partitions of 1G size each.
- create a script that will devide the disk into 3 primary partitions with 3rd being the biggest out of 3.
- create a script that will take a required disk name and will make extended partition type and will devide it into 8 partitions.
	- script also should put a filesystem on each of the patitions: ext4 if not defined elsewise.
- create a script that will create new 1 gb partition, using btrfs file system.
  - also add a 128 mb swap partition and mount this persistentrly.

- create a script that will setup __file based swap__ file with dd
  - have the script `mount` it persistently on to your system.

---

### Lab 2

- create a script that will require disk names and will automation creations of Logical Volume Management.
- create a script that will automate your LVM resizing procedure.
	- validate that filesystem is not corrupted after the automation procedure.
	- auto mount the disks after they are done.

---

### Lab 3
- create new repo at github/gitlab/bitbuket called `pancake_storage`
- create a script that will several Funcitons:
  - a. Function that will check  and list all possible  block devices.(do not Use commands for device listing)
  - b. Function that will change filesystem according to paramenters provided by Users request.(Use select loop to choose a type of filesystem: ext3/etx4/ntfs/jfs.. so on and so forth)
  - c. Function that will change partion as requested by User.(pass the values to parted/fdisk)

---

# links to exercise
- https://www.tecmint.com/parted-command-to-create-resize-rescue-linux-disk-partitions/

---