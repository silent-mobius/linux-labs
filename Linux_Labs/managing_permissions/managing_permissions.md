# Managing Permissions

## Lab 1
###### NOTE
 You must undestand Users and groups to handle this lab.

- Create directories hr and management in /data folder.
- Members of the management group should should be able to read and Write to hr directory.
- No other User should be able to access to these directories.
- Users will only be able to delete files.

---

### Lab 2
Create repository at github/gitlab/bitbuket called Shell Per Mission

- Create a script that will:
  - Check that your User is in sudo/wheel group of Users.
  - Check that your home directory is at /home.
  - Check your nick name is Set.

- Create a script that will:
  - Creates a hidden file named User_info.sh.
  - Puts your details in it: name, lname, ID, DOB, POB.
  - Make the the file Print every time you login.(check about .bashrc file in your home folder.)

- Create script that will:
  - Use "for" loop to see all the "services" in /etc/systemd/system.
  - If there are other types of files (target/wants....etc) put them in file under /tmp folder, in file named "systemd.misc".
  - Create a script that will Print triangle using "*" sign.
  - Create a script that will Print reverse triangle using "*" sign with "for" loop.
  - Create a script that will Print diamond (triangle + reverse triangle) using "*" with "for" loop.
  - Create a script that will Use "df" command and "for" loop will Print only disks with low space.
  - Create a script that will Print chess board (black/white cubes).



`Please read in "linux_fun.pdf" chapters 22, 23, 24, 25. Please also read "shell_scrpting_with_bash.pdf" till page 38.`

---