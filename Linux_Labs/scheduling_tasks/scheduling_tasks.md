# Scheduling Tasks

## Lab 1

- Use the appropriate command to Create a file /tmp/testfile at 7 minutes from now.
- User the appropriate solutions to Run the fstrim command on daily basis.
- Ensure that as User 'student' on a daily basis, at 4 pm the message 'konichiwa' is written to syslog

---