# Managing Software
###### NOTE
 Do each lab in clean environment. (in new machine in case it was not clear!)

## Lab 1

- On Debian based system:
  - Install basic development tools.
  - Install Vim editor for editing files.
  - Check which version of gcc you have on your system.
  - Install docker by using remote installation script.
  - In case you are doing the lab at GUI based machine:
  - Install multi-terminal tool called `teminator`.
  - Install Geany with its all plugins for graphical development.

---

## Lab 2

- On RedHat based system:
  - list all possible groups for installation.
  - check if gcc is installed.
  - Install Development Tools and Development Libraries groups with one command.
  - if python groups are available install them as well.

--- 

## Lab 3

- On Debian and On RedHat based system.
  - Download from python's site python source files.
  - Open it in designated folder.
  - Validate that you have all files needed to compile the source files.
  - Compile the source files and link them.
  - install it without damaging the system.(alternative installation)rtfm

---

## Lab 3

- Create repository at github/gitlab/bitbuket called it Greedy Pit and save next scripts in it.
  - Create a script that Setups developement environment for ruby.
    - Install ruby and ruby dependancy mangement(gems).
    - Install ruby on rails framework.
    - Install sqlite3 database.
    - Run ruby on rails to validate that it works.

---

## Lab 4

- Create a repository called pio-pio-cake and save in it the nex scripts.
  - Create a script that will install LAMP server:
    - Linux
    - Apache web server
    - MariaDB databese
    - Php5 and php7 language support
    - Php language based framework called phpcake
  - create basic phpinfo() file that shows all required php libs installed on the server.

---
