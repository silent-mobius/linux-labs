
# Managing Logical Volumes

## Lab 1

- Verify that you have enough disks for lvm.
- Change partition types to work as lvm's.
- Add the disks on physical volume.
- Let's revert this last action for practice purposes.
- List all disks on physical Volume. to learn about the current state of your physical volumes

---

## Lab 2

- Add the disks on physical volume.
- Now let's Create our volume group `fileserver` and add all disks.
- Learn about our volume groups.
- Scan if there are any other volume groups.
- For practice purposes let's rename our volumegroup `fileserver` into `data`.

---

## Lab 3

- On your second hard disk, add 1 gb lvm format it with the ext4 file system and mount persistently `/books`.
- Shrink the lvm logical volume you've just Created to a size of 800 Mib, without loosing any data currently.

---