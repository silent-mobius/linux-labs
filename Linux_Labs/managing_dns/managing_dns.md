# Managing DNS

## Lab 1
- Set up a cache-only DNS nameserver which forwards to and DNS server of your choice.
- Configure a DNS resolver on a second machine to Use the cache-only nameserver
- Verify the workings using dig.

---