<center style="background-color:lightgreen">
    <h1 style="color:black"> 
     Linux-Labs
    </h1>
</center>

[Please Read License](#License)

[Managing an FTP server](#managing-an-ftp-server)

[Managing DNS](#managing-dns)

[Managing Networking File Sharing](#managing-networking-file-sharing)

[Managing Mariadb](#managing-mariadb)

[Managing Postfix](#managing-postfix)

[Managing Proxy](#managing-proxy)

[Managing KVM Virtual Machines](#managing-kvm-virtual-machines)

[General Shell Scripts](#general-shell-scripts)

---

## `Linux Install`

### Lab 1

- Install Linux distro on your laptop/desktop/machine
- Install virtualization service.
- Install RPM based distro.
- Install APT based distro.

### Lab 2

- Use Kickstart file generator to Create your own  custom system for remote Install.
- Setup PXE system that will generate connection and will serve your kickstart file.



---

## `Using Linux Essential`

### Lab 1

- Find out which vesion of kernel your computer is using.
- Ising man and related resourse to find out how change the hostname.
- Read the help output for lvCreate and find which options must be Used.


---

## `Using File Management Tools`

### Lab 1

- Create a folders `code`,`dev` and `ops` at `/tmp/data`.
- Copy all files that start with a,b or c from `/etc/` to  `/tmp/data`.
- From `/tmp/data`, move all files that have a name starting with an a or b to `/tmp/data/dev` and all other files `/tmp/data/code`.
- Find all files in `/etc` that have a size smaller than 1000 bytes and copy those to   `/tmp/data/ops/`.
- Create  symbolic link to `/var` at `/tmp/data`.

---

## `Working With Text Files`

### Lab 1

- Display the 5th line of passwd file.
- Do the same with different file.
- Show first column of `ps aux`.
- Catch all the files in `/etc/` that end with `.Conf`.
- Get name of all files that contain at most 4 letters.
- Filter out from search output the name `User` but, validate that User `root` is not in the list. 

## `Connecting To A Server`

### Lab 1

- Log to your system using text-only console.
- Log to your system, from your system via ssh.
- Check out which terminal name are Used for all logins.


---

## `Hardware Check and Monitor`

### Lab 1
- list all block devices.
- list which of devices are mounted.
- list which of the device are mounted and how much of them is taken.
- list all the usb devices on the system.
- list all the pci devices on the system ang catch the display device.


### Lab 2
- Write a script that:
  - a. has a Function that lists all block devices.
  - b. has a Function that lists all network devices.
  - c. main Function that gets a parameter of User, that checks if he exists.
  - d. loop that will ask what to do.


Please read about regex in the linux_fun.pdf: chapter 21
Please read about proccesses and networking in the linuxsys.pdf: chapters 1,2,3,21,22,23,24

links:

- https://www.grymoire.com/
- https://www.theunixschool.com/
- https://www.tutorialspoint.com/awk/
- https://www.tutorialspoint.com/sed/

---

## `Managing Users And Groups`

### Lab 1

- Create 5 Users: Bruce, Barry, Diana, Courtney and Kara.
- Use input redirection, to Set password for all Users as 'P@ssw0rd'.
- Set their passwords to expire after 60 days.
- Create group jla and make Bruce,Diana,Kara and Barry part of it.
- Create group `b.inc` and make Diana, Bruce and Courtney part of it.
- Create another `heros` and add all the new Users to it.
- Ensure all Users get home directory at `/home/`.
- Set a limit on Users to Run up to 15 processes at the same time.

### Lab 2

- Create a project on github/gitlab/bitbuket called `regExUp`.
  - a. Create a script that will take a list of Users and will add them to your system.
  - b. The script should have a validation Function: it will check if User with that Username or nickname exists.(Use regex)
  - c. If User exists, script will stop and it will give grandioUse output that User or nickname already exists.
  - d. If User does not exists, script will continue and it will give grandioUse output that User or nickname is being Created.
- Create a script that will display a list of all the Users on your system who log in with the Bash shell as a default. (regex and /etc/skel/)
- Create a script that checks who was the last User that Created file /tmp and validate that User with /etc/passwd.(regex and /etc/passwd)

Please read links:
https://www.regexone.com/ - regex tutorial.
https://regexper.com - try testing various regex via this link.

Please chapters for next lesson: 
linuxsys.pdf -- 4,5,6,7,8,9,10,11,18,19,26,30

### Labs 3


- Add the Users: tstart, bwayne, dprince to the Server
- Create the `superhero` Group
- Set `wheel` Group as the the `tstark` Account's Primary Group
- Add `superhero` as a Supplementary Group on All Three Users
- Lock the `dprince` Account


<!--
useradd <username>

So we would run

useradd tstark

useradd cdanvers

useradd dprince
-->
<!--
To create a new group we would run

groupadd <groupname>

So for this task

groupadd superhero
-->
<!--

For this task we would run usermod like this

usermod -g wheel tstark
-->
<!--
There isn't an easy way to do this all at once, so we need to run the following command for each user

usermod -aG superhero <username>

So

usermod -aG superhero tstark

usermod -aG superhero dprince

usermod -aG superhero cdanvers
-->
<!--
To lock an account all we have to do is run:

usermod -L dprince
-->

### Lab 4


- Create two new users.
- Verify the `/etc/sudoers` file and test access.
- Set up the web administrator.

<!--

    Create two new users on the system, and assign the avance user to the wheel supplemental group:

    sudo useradd -m gfreeman
    sudo useradd -G wheel -m avance

    Set the password for both accounts to LASudo321:

    sudo passwd gfreeman
    sudo passwd avance


    Using the visudo command, verify that the /etc/sudoers file will allow the wheel group access to run all commands with sudo. There should not be a comment (#) on this line of the file:

     %wheel  ALL=(ALL)       ALL

    From the cloud_user login, use the su (substitute user) command to switch to the avance account, and use the dash (-) to utilize a login shell:

     sudo su - avance

    As the avance user, attempt to read the /etc/shadow file at the console:

     cat /etc/shadow

    As a regular user, avance does not have sufficient privileges to do so. Rerun the command with the sudo command:

     sudo cat /etc/shadow

    After you have verified that avance can read the /etc/shadow file, log out of that account:

     exit


Now we need to configure gfreeman's account to have the ability to restart or reload the web server when needed. Since he will be the webmaster, he needs sudo permissions to restart the service.

    First, create a new sudoers file in the /etc/sudoers.d directory that will contain a standalone entry for webmasters. Use the -f option with the visudo command to create this new file:

     sudo visudo -f /etc/sudoers.d/web_admin

    Enter in the following at the top of the file. This will create an alias command group that we can apply to any user or group that we add to this file. This group of commands will contain the necessary commands for restarting or reloading the web server:

    Cmnd_Alias  WEB = /bin/systemctl restart httpd.service, /bin/systemctl reload httpd.service

    Add another line in the file for gfreeman to be able to use the sudo command in conjunction with any commands listed in the WEB alias:

     gfreeman ALL=WEB

    Save and close the file.

    Next, log into the gfreeman account:

     sudo su - gfreeman

    Attempt to restart the web service:

     sudo systemctl restart httpd.service

    Now gfreeman can restart the web server. As the gfreeman user, try to read the new web_admin sudoers file with the sudo command:

     sudo cat /etc/sudoers.d/web_admin

    Since the cat command is not listed in the command alias group for WEB, gfreeman cannot use sudo to read this file.

-->

### Lab 5

- Enable SSH to Connect Without a Password from the `dev` User on `server1` to the `dev` User on `server2`
- Copy All tar Files rom `/home/dev/` on `server1` to `/home/dev/` on `server2`, and Extract Them Making Sure the Output is Redirected to `/home/dev/tar-output.log`
- Set the Umask So That New Files Are Only Readable and Writeable by the Owner
- Verify the `/home/dev/deploy.sh` Script Is Executable and Run It



<!--
We need to use SSH keys to satisfy this requirement, so generate them with this:

 [user@host]$ ssh-keygen

Then run:

 [user@host]$ ssh-copy-id <server2 IP>

---

First we need to get into the 'dev' user:

[user@server1]$su dev 

The Password is the cloud_user password

Then we need to move to the /home/dev/ directory:

[dev@server1 ~]$ cd /home/dev

We need to use a method of copying files over a network. scp is the best tool, like this:

[dev@server1]$ scp *.gz <server2 IP>:~/

Then connect to server2 using ssh:

[dev@host]$ ssh <server2 IP>

Then we can extract the files:

[user@server2]$ tar -xvf deploy_content.tar.gz >> tar-output.log
[user@server2]$ tar -xvf deploy_script.tar.gz >> tar-output.log

Make sure to use >>, so that the output is appended rather than overwritten.
---

The task is asking to make new files with the following permission: 0600

So we can do subtraction to figure out what our umask should be.

0666 <-- Default

0600 <-- Desired

----

0066 <-- What we need to set

So we run umask 0066

---

First we check permissions on deploy.sh:

 [user@server2]$ ls -l deploy.sh
 -rw-r--r--. 1 dev dev 151 Mar 27 15:11 deploy.sh

There's no execute bit. Let's add one:

 [user@server2]$ chmod +x deploy.sh

And then run it:

 [user@server2]$ ./deploy.sh

----
#!/bin/bash

m=$(umask)

if [[ $(umask) != "0066" ]]; then
	echo "umask is set incorrectly. Please fix and then re-run."
	exit 1
fi

touch /tmp/itran

----
-->

### Lab 6

##### **note** verify that server is up first.
- Set the Server up to Authenticate Using _ldap.example.com_
- Get the UID of `ldapuser3`


<!--
-------------
Additional Information and Resources

The LDAP server is at ldap.linuxacademy.com.

The TLS Certificate is at http://ldap.linuxacademy.com/pub/cert.pem.

The LDAP Manager is cn=Manager,dc=linuxacademy,dc=com.

The LDAP Manager's password is 123456.

The password for ldapuser1, ldapuser2, and ldapuser3 is 123456.

You can use X11 forwarding if you wish to run an application like authconfig-gtk.
-----------

The first step is to install the required packages:

yum install nss-pam-ldapd pam_krb5 autofs nfs-utils openldap-clients

Once that is complete we can run:

authconfig --enableldap --enableldapauth --enablemkhomedir --enableldaptls --ldaploadcacert=http://ldap.example.com/pub/cert.pem --ldapserver=ldap.example.com --ldapbasedn="dc=example,dc=com" --update

Alternatively, we could install authconfig-gtk and then run the export as root to make the same settings.

export XAUTHORITY=/home/cloud_user/.Xauthority

yum install authconfig-gtk

authconfig-gtk

Note: you will need to use ssh- X to connect in order to use authconfig-gtk.



Once authentication is complete we can run:

id ldapuser3

to get the result. We can also, as root, run su - ldapuser3 and run id.

-->

---

## `Managing Permissions`

### Lab 1

**NOTE** you must undestand Users and groups to handle this lab.

- Create directories `hr` and `management` in `/data` folder.
- Members of the `management` group should should be able to read and Write to `hr` directory.
- No other User should be able to access to these directories.
- Users will only be able to delete files.

### Lab 2
Create repository at github/gitlab/bitbuket called `Shell Per Mission`
- Create a script that will:
  - Check that your User is in sudo/wheel group of Users.
  - Check that your home directory is at /home.
  - Chat your nick name is Set.
- Create a script that will:
  - Creates a hidden file named User_info.sh.
  - Puts your details in it: name, lname, ID, DOB, POB.
  - Make the the file Print every time you login.(check about .bashrc file in your home folder.)
- Create script that will:
  - Use "for" loop to see all the "services" in /etc/systemd/system.
  - If there are other types of files (target/wants....etc) put them in file under /tmp folder, in file named "systemd.misc".
- Create a script that will Print triangle using "*" sign.
- Create a script that will Print reverse triangle using "*" sign with "for" loop.
- Create a script that will Print diamond (triangle + reverse triangle) using "*" with "for" loop.
- Create a script that will Use "df" command and "for" loop will Print only disks with low space.
- Create a script that will Print chess board (black/white cubes).


Please read in "linux_fun.pdf" chapters 22, 23, 24, 25.
Please also read "shell_scrpting_with_bash.pdf" till page 38.


---

## `Managing Networking`

### Lab 1

- Configure your hosts with persistent address Configuration, matching the subnet that is Used in your network. choose any ip address, as log as it allows you to connect to other networks.
- Configure ipv6 as well.

### Lab 2
- Write a script that will :
  - a. Print out all interfaces on your computer and ip of that interface.
  - b. If the interface lacks ip address - Create a Function that will guide you though the ip Configuration steps.(Set ip - mandatory, Set netmask - mandatory, Set gateway- mandatory, Set dns - not mandatory)
  - c. Write a Function that will check what type of linux disto you are Running.

<!-- 
### Lab 3

Additional Information and Resources

You will need to populate the Network.txt file located in the cloud_user's home directory with the requisite information.
Learning Objectives

Add the IP Address to `network.txt`


You may use NetworkManager (nmcli) to locate the information you need.

Add the Netmask to `network.txt`


You may use NetworkManager (nmcli) to locate the information you need.

Add the Gateway IP to `network.txt`


You may use NetworkManager (nmcli) to locate the information you need.

Add the DNS Domain to `network.txt`


You may use NetworkManager (nmcli) to locate the information you need.

Locating the Network Information
Introduction

Being able to locate the IP address, netmask, gateway, DNS nameserver, and domain is critical to understanding Linux networking. In this hands-on lab, we're going to populate a text file with this information.
Logging In

Use the credentials provided in the hands-on lab overview page to log into the lab server with SSH.
Getting Comfortable

Once we're in, we've got to get a few pieces of information added to a network.txt file in our home directory. We're going to use an nmcli command each time. We may want to have two terminals open, one to edit the text file (with whichever text editor we want) and the other to run commands and get output.

First, let's see if NetworkManager is even running:

[cloud_user@host]$ systemctl status NetworkManager

That will show us that is it. Now let's get a list of active connections.

[cloud_user@host]$ nmcli c show

We'll see a line for eth0. That's the one we're going to be querying.
Get the Information

This is actually pretty easy. Let's run this one command:

[cloud_user@host]$ nmcli d show eth0

Conclusion

The information we need is in this output. The IP address and netmask (IPV4.ADDRESS[1]), gateway address (IPV4.GATEWAY), DNS server (IPV4.DNS1), and DNS domain (IPV4.DOMAIN[1]) are all there. To get a passing grade on the lab, just get those values from the command output and get them into the proper lines of the network.txt file. Then we're done. Congratulations!

-->
--- 

## `Configuring The SSH Service`

### Lab 1

**NOTE** Use at least 2 machines for this lab.
- On server one, Configure the ssh service in such way that allows root User to connect.
- From the other server, copy the file /etc/hosts to the home directory of the root User.

### Lab 2
<!--

Additional Information and Resources

The development team in your organization is setting up their new development servers in preparation for the creation of a new web-based API. They are going to be creating configurations, copying files, etc. between two servers using a single service account.

You have been provided with credentials and connectivity information to those two new server instances. The service account they wish to use is the cloud_user account that you were provided. Following company security policy, a complex password has been set that is making periodic connections, copies, and service configurations hard for the team. They have asked you to simplify the process and create a trust for the service account between the two systems.

To make the trust between the two systems, you have determined the easiest method for doing so while maintaining security would be to use SSH keys and exchange them between the systems. You will need to create keys on both servers for the cloud_user service account and then exchange each server's account key with the other. This will facilitate connections from one system to the other and the reverse, regardless of the initiating system. Once you verify each user can log in from one server to the other, you can turn them back over to your team for use.
Learning Objectives

Create the SSH Keys on Server 1 and Server 2.


    Generate a new key on server 1.

     ssh-keygen

    Generate a new key on server 2.

    ssh-keygen


Exchange the Keys between Server 1 and Server 2.


The student will need to exchange the cloud_user service account keys created previously with the matching account to and from SERVER ONE and SERVER TWO.

    Copy the contents of id_rsa.pub to the clipboard.
    Connect to server 2.
    Append the public key to the authorized_keys file.
    Generate a new key on server 2.
    Copy the contents of the id_rsa.pub to the clipboard.
    Connect back to server 1.
    Append the public key to the authorized_keys file.


Introduction

Understanding the creation and exchange of SSH keys is a key concept to grasp as a new system administrator. In this lab, we will generate keys on two systems using the ssh-keygen utility and learn how to exchange and verify the keys with a remote system using ssh-copy-id and associated key files on each. At the end of this lab, you will understand how to create secure keys for remote access, how to exchange them, and where to store them on each system involved in the chain.
Create the SSH Keys on Server 1 and Server 2
Create the Key on Server 1

    In your terminal, log in to Server 1.

    ssh cloud_user@<SERVER1_PUBLIC_IP>;

    List the contents of the current directory.

    ls -la

    Change to the .ssh directory.

    cd .ssh

    List the contents of the .ssh directory.

    ls -la

    Generate a key for Server 1.

    ssh-keygen

    Press Enter at the next three prompts.
    List the contents of the .ssh directory again.

    ls -la

    List the contents of the id_rsa.pub file.

    cat id_rsa.pub

    Copy the output of this command to your clipboard.

Create the Key on Server 2

    Log in to Server 2.

    ssh cloud_user@<SERVER2_PUBLIC_IP>;

    Change to the .ssh directory.
    List the contents of the .ssh directory.

    ls -la

    Install the nano text editor.

    sudo yum install nano

    Enter your password at the prompt.
    Open the authorized_keys file in nano.

    nano authorized_keys

    Add the key we just generated to the file.
    Press Ctrl + X.
    Press Y** then **Enter to save the changes.

Exchange the SSH Keys between Server 1 and Server 2

    In your Server 2 terminal window, create a new key.

    ssh-keygen

    Press Enter for the next three prompts.
    List the contents of the current directory.

    ls -la

    List the contents of the id_rsa.pub file.

    cat id_rsa.pub

    Copy the output of this command to your clipboard.
    Type exit to log out of Server 2.
    Install nano.

    sudo yum install nano

    Type y to continue.
    List the contents of the current directory.

    ls -la

    Open the authorized_keys file in nano.

    nano authorized_keys

    Add the key we just generated to the file.
    Press Ctrl + X.
    Press Y** then **Enter to save the changes.

Test the Configuration

    Attempt to log in to Server 2 from Server 1 without a password.

    ssh cloud_user@<SERVER2PUBLIC_IP>; 

    Attempt to log in to Server 1 from Server 2 without a password.

    ssh cloud_user@<SERVER1PUBLIC_IP>; 

Conclusion

Congratulations, you've successfully completed this hands-on lab!
-->

### Lab 3
 x forwarding


### Lab 4
ssh tunnel

---

## `Configuring The Firewall`

### Lab 1

- Use iptable to build a firewall that allows the following: 
    - Incoming: ssh, ping.
    - Outgoing: ssh, dns, ping http.

--- 

## `Managing Time`

**NOTE** Use at least 2 machines for this lab
- Configure vm as time server, using the protocol of your selection. before that, make sure that system time and hardware are synced.
- Configure another vm (or 2) as ntp client. monitor the sync status on the client.

---

## `Managing Processes`

### Lab 1 

- From a root shell, start 3 process of sleep 50000 as background job.
- Use the appropriate comand to show theat they are Running as expected.
- Change process priority so that one of those jobs gets double the amount of CPU resource.
- Terminate all 3 processes.


### Lab 2

- Create new project called `merrie array melodies` at github/gitlab/bitbuket and keep the scripts there.
- Create a script that will have Functions:
  - a. Function to get data and to be save them in array.
  - b. Function that will go throught the data and get every element with digits in it, for every iteration add a sleep command of 1 second.
  - c. Function that will Print out the waiting bar while the above Function (b) is Running.
  - d. All Functions above must Run in Function called main.
  - e. Main Function may Run only if root User is Running it.

---

## `Managing Software`

**NOTE** Do each lab in `clean` environment. (in new machine in case it was not clear!)

### Lab 1
- On Debian based system:
  - Install basic development tools.
  - Install Vim editor for editing  files.
  - Check which version of gcc you have on your system.
  - Install docker by using remote installation script.
- In case you are doing the lab at GUI based machine:
  - Install multi-terminal tool called teminator
  - Install Geany  with its all plugins for graphical development.

### Lab 2
- On RedHat based system:
  - list all possible groups for installation.
  - check if gcc is installed.
  - Install `Development Tools` and `Development Libraries` groups with one command.
    - if python groups are available install them as well.

### Lab 3
On Debian and On RedHat based system.
- Download from [python's site](python.org) python source files.
- Open it in designated folder.
- Validate that you have all files needed to compile the source files.
- Compile the source files and link them.
- install it without damaging the system.(alternative installation)[rtfm]()


### Lab 3
- Create repository at github/gitlab/bitbuket called it `Greedy Pit` and save next scripts in it.
- Create a script that Setups developement environment for ruby.
  - Install `ruby` and ruby dependancy mangement(`gems`).
  - Install `ruby on rails` framework.
  - Install sqlite3 database.
  - Run `ruby on rails` to validate that it works.

### Lab 4
- Create a repository called `pio-pio-cake` and save in it the nex scripts.
  - Create a script that will install LAMP server: 
    - Linux
    - Apache web server
    - MariaDB databese
    - Php5 and php7 language support
    - Php language based framework called `phpcake`
    - create basic phpinfo() file that shows all required php libs installed on the server.


---

## `Scheduling Tasks`

### Lab 1

- Use the appropriate command to Create a file /tmp/testfile at 7 minutes from now.
- User the appropriate solutions to Run the fstrim command on daily basis.
- Ensure that as User 'student' on a daily basis, at 4 pm the message 'konichiwa' is written to syslog

---

## `Configuring Logging`

### Lab 1

- Configure rsyslog to Write all messages with a priotiry of `crit` to /var/log/crit.
- Make sure this file is rotated on a daily basis.
- Use logger to test this working.

---

## `Managing The Linux Kernel`

### Lab 1

- Request a list of current loaded linux kernel modules.
- Load the module for vfat support; does it have any dependencies.
- Unload this module.
- Make sure system can be Used to forward your ip packets.

---

## `Managing The Boot Process`

### Lab 1
Your developers share a system that utilizes a desktop environment for the IDE tools. The other system administrator changed the environment so that it only boots into a command line shell, just before he went on vacation. You will need to correct the default boot environment.
- Check what is the current default target. Use the appropriate command to verify this.

<!-- systemctl get-default -->
- Change the default target to Graphical
  - You'll need administrator permissions to change the default target so that the computer boots into a graphical desktop.

<!-- sudo systemctl set-default graphical.target -->

### Lab 2
<!--
During the time that your development team has spent working on the new Web-based API for your organization, there have been several instances of mistaken keystrokes or processes that have necessitated the restoration of the site directory from backup.

You have been asked to run periodic backups of the website directory and, given that the development environment does not have access to the backup network, you have decided to write a custom service that will do so.

You have previously written the systemd unit files to back up the site. You have been provided a file called web-backup.sh in your /root directory (NOTE: you will need to become the root user to complete your work). Using that file and the associated web-backup.service, create a systemd timer unit file that will control the schedule of your service.

After you have all three components ready, stage the files in their appropriate locations and start the service for your team and turn it back over for their use.
Learning Objectives

Service timer unit file exists.


Follow the scenario to create the appropriate time file in the indicated directory.
Custom service is running.


Once the backup service has been created successfully from the scenario, make sure it is running so the backup will happen as scheduled.
!!!!!!!!!!!!!!!!!!!!!
cat web-backup.service 
[Unit]
Description=Backup the web site, every day, or the boss will be mad

[Service]
Type=simple
ExecStart=/usr/local/sbin/web-backup.sh

[Install]
WantedBy=multi-user.target
!!!!!!!!!!!!!!!!!!!!!!!!

cat web-backup.sh
#!/bin/bash

DATE=$(date "+%d")

/bin/tar -czf /root/site-backup-$DATE.tar.gz /var/www/html
!!!!!!!!!!!!!!!!!!

 cat web-backup.timer 
[Unit]
Description=Fire off the backup

[Timer]
OnCalendar=*-*-* 15:00:00
Persistent=true
Unit=web-backup.service

[Install]
WantedBy=multi-user.target
!!!!!!!!!!!!!!!!!!!!!!!!


-->

### Lab 3 


<!--
During the development of the new web-based API your organization has been working on, the web server they are using has stopped responding to web requests.

The development team indicates that this happened after they made some changes to the web server configuration and restarted the service. The web server stopped at that point and they were unable to restore the service.

As the system administrator on this system, you will need to investigate the reasons for the failure by looking at the approprite service log file. Once you have determined the issue, restore the service and then submit the server back to the development team to continue their work.
Learning Objectives

Check the Web Server Configuration File


The Apache website's default configuration file is missing. Once this task is completed, the service will start as expected.

Verify That the Web Server Service Is Running


Once the configuration issue is resolved, make sure the appropriate service is running.


Working with System Service Log Files Using the Journal Control
Introduction

Troubleshooting is an important part of working with services through systemd. In this hands-on lab, we will learn how to view system service log files using the Journal Control utility. At the end of this hands-on lab, you will know how to use the built-in journalctl utility to view and troubleshoot system services.
Connecting to the Lab

    Open your terminal application, and run the following command. (Remember to replace <PUBLIC_IP> with the public IP you were provided on the lab instructions page.)

    ssh cloud_user@<PUBLIC_IP>;

    Enter your password at the prompt.

Check the Web Server Configuration File

    Change to the root account.

    sudo su -

    Check the status of the web service.

    systemctl status httpd.service

    Attempt to start the web service.

    systemctl start httpd.service

    After the service fails to start, check the journal.

    journalctl -u httpd.service

    Check the directory where the httpd configuration file should be.

    ls /etc/httpd/conf

    Restore the original httpd configuration file.

    mv /etc/httpd/conf/httpd.conf.bkup /etc/httpd/conf/httpd.conf

    Restart the service.

    systemctl restart httpd.service

Verify That the Web Server Service Is Running

    Check the status of the service.

    systemctl status httpd.service

    Navigate to the local web page.

    elinks http://localhost

Conclusion

Congratulations, you've successfully completed this hands-on lab!


-->

### Lab 4

- On a server with graphical environment Installed: reboot
- From the GRUB boot prompt, start the text-only target.
- From text-only target, boot to the graphical target
- Ensure the your ssh service will be automatically started, andn that after a failure it will be restarted in 3 secunds.





---

## `Managing Mandatory Access Control`

### Lab 1

- Ensure that your server is protected through mandatory access control.
- List all the object that are under mandatory access control.

---

## `Managing Partitions And File Systems`

### Disk partitions

### Lab 1

###### NOTE: verify that disks are mountable.

- create a script that will take a required disk name and will delete all current paritions.
- create a script that will take a required disk name and will create 2 primary partitions of 1G size each.
- create a script that will devide the disk into 3 primary partitions with 3rd being the biggest out of 3.
- create a script that will take a required disk name and will make extended partition type and will devide it into 8 partitions.
	- script also should put a filesystem on each of the patitions: ext4 if not defined elsewise.
- create a script that will create new 1 gb partition, using btrfs file system.
  - also add a 128 mb swap partition and mount this persistentrly.

- create a script that will setup __file based swap__ file with dd
  - have the script `mount` it persistently on to your system.

---

### Lab 2

- create a script that will require disk names and will automation creations of Logical Volume Management.
- create a script that will automate your LVM resizing procedure.
	- validate that filesystem is not corrupted after the automation procedure.
	- auto mount the disks after they are done.

---

### Lab 3
- create new repo at github/gitlab/bitbuket called `pancake_storage`
- create a script that will several Funcitons:
  - a. Function that will check  and list all possible  block devices.(do not Use commands for device listing)
  - b. Function that will change filesystem according to paramenters provided by Users request.(Use select loop to choose a type of filesystem: ext3/etx4/ntfs/jfs.. so on and so forth)
  - c. Function that will change partion as requested by User.(pass the values to parted/fdisk)

---
# links to exercise

- https://www.tecmint.com/parted-command-to-create-resize-rescue-linux-disk-partitions/

---

## `Managing Logical Volumes`

### Lab 1

- Verify that you have enough disks for lvm.
- Change partition types to work as lvm's.
- Add the disks on physical volume.
- Let's revert this last action for practice purposes.
- List all disks on physical Volume. to learn about the current state of your physical volumes

### Lab 2

- Add the disks on physical volume.
- Now let's Create our volume group `fileserver` and add all disks.
- Learn about our volume groups.
- Scan if there are any other volume groups.
- For practice purposes let's rename our volumegroup `fileserver` into `data`.

### Lab 3

- On your second hard disk, add 1 gb lvm format it with the ext4 file system and mount persistently `/books`.
- Shrink the lvm logical volume you've just Created to a size of 800 Mib, without loosing any data currently.

---

## `Managing Software RAID`

### Lab 1

- Create RAID 5 device, using 3 disk devices of 1 GB each. Also allocate and an additional device as spare device.
- Put a file system on it and mount it on `/raid`.
- Fail one of the devices,monitor what is happening.
- Replace the failed with the spare device.

---

## `Managing Web Services`

### Lab 1

- Install web server. After connecting to the web server, a client should see the message "Welcome to my web server"

---

## `Managing an FTP server`

### Lab 1

- Install an FTP server that offers anonymous file download. Ensure that this server is accessible from other machines.


--- 

## `Managing DNS`

### Lab 1
- Set up a cache-only DNS nameserver which forwards to and DNS server of your choice.
- Configure a DNS resolver on a second machine to Use the cache-only nameserver
- Verify the workings using dig.

---

## `Managing Networking File Sharing`

### Lab 1

- One one server, Configure samba to share a directory `/samba`. Also, Configure NFS to share a directory `/files`.
- On other server, mount these shares persistently.

---

## `Managing Mariadb`

### Lab 1

- Create a simple database with the name cities. in this database, add a table that contains a "city" and a "country" fields. insert some data. to test the working, make a query to request to request all cities in a specific country that you've inserted into the database. 

---

## `Managing Postfix`

### Lab 1

- Set up your server to forward email messages to an SMTP serer of your choice.
(**notice that this may not work, as the destination email server needs to accept your relaying messages to it.**)

---

## `Managing Proxy`

### Lab 1

- Install squid on your machine.
- Configure a basic Squid web proxy and tests it is working.

---

## `Managing KVM Virtual Machines`

### Lab 1

- Install KVM  virtual machines with minimal Installation of the Linux distribution of your choise.
- Use `virsh` command to stop this virtual machine and start it again.


---

## `General Shell Scripts`


### Lab 1

- Create project named "baby_steps" in github/gitlab/bitbuket and push all scripts to them
- Create a file  and in it Write commands that will Run sleep command for 5.5 seconds and will Print working directory.
  - Run the file with "bash" command -- bash file_with commands_in_it.
- Copy the previous file and make several changes: change time to 2.8 seconds and after every sleep command Create empty file with alfabetical letter. for example: file_a, file_b, file_c ....etc,etc.
  - Run the file with "bash" command.
- Create a file that will hold variable "name" with value of you first name, after that Run sleep command for 4 seconds and Print it to standard output.
  - Run the file with "bash" command.
- Copy the file with diffrent name add to it additional variables of : lname, ID, DOB(date of birth), COB(country of birth) and later Print them to standard output.
  - Run the file wiht bash command, but redirect its output  to tmp file in your home folder.
- Create a file that will Run "mkdir" with parenthood to folder that is named after you. after it is Created change your location in to it.
  - Run the file with bash command.
  - Run the file again but this time redirect its output in to tmp file in /tmp directory.
- Create a file that Runs "find" commands and seeks all the files with "tmp" in its name.
- Run the file with "bash" command and redirect its output  to /dev/null
- Try to read the output in /dev/null with less command.
- Let me know if you have succeded.
- Try to Use locate command to do the same and filter the output with "grep","sed" and "awk" commads.
- Let me know if you have succeded.
  - Create a file that will Print all system defined variables and their values (DO NOT Use "Set" OR "env" commad -- do them manually.
- Run the file with "bash" command, pipeline it and then with "cut" command get all the 2nd field from output.
- Do the same as in pervious task, but Use "awk" command Instead.



- To read in linuxfun.pdf - again if not have done so -- chapters 13,14,15,17,18.
- To read in linuxfun.pdf - chapters 19, 20, 21
- Go to [Vim-Advantures]("https://vim-adventures.com/") and play until you get bored.


### Lab 2

- Create a new project in github/gitlab/bitbuket called `Exec and monitor`.
  - Create a folders called "bin" and "sbin" at the repository folder.
  - In bin Create a script that will  list all processes ran by your User. should Run with loop(not with top command)
  - Create a sccript that will stop previous one but won't kill the process.
  - All the scripts before should keep logs in /var/log/script folder. validate that the folder exists.

-  In sbin 
  - Create a script that will list all processes ran by root User. should Run with loop(not with top command)
  - Create a sccript that will stop previous one but won't kill the process.
 
- Create a script in bin, that will have a Functions:
  - a. to see all connected hardware.
  - b. to show all opened ports.
  - c. to show all connected Users.
  - d. to show status all storage. 
  - e. to Run all Functions and save output in logs.

- Create a script that will show menu that will execute all previous script mentioned above. 

### Lab 3

- Create a new project in github/gitlab/bitbuket called `aunt_Mary's_cake_of_checks`:
  - Create a script that will scan whole os and will count amount of files that belongs to your User.
  - Add to prvious script validation - only User with your ID can Run script all the others should be denied.
  - Another feature to add the script: count also all hidden files in whole file system.
  
- Create a script that will Run every time you Use "chmod commend": the script needs to be invoked as alias.
  - Script should ask you if you are sure you would like to make these changes/
  - After "chmod" command finishes to Run, User should see the summary of what was done: the name of the file that had permissions changed, date of the change and User who changed it. - it also needs to be inserted as an alias. (to Set an alias edit one of the bashrc files and the source the file.)
- Create a script that will receive a list of Users. name of group  and list of aliases that they need and then will  Created them.
  - If the script will be Run with other User than root, it should be notified to User and also kept in a log file that should be Created in "/tmp" folder under a name of the script with ".log" suffix  (for example: script_name.log)
- Create script that will recieve a list of folder names and with the same single command : it will Create those folders and also will give them permissions of 555.

- Create a script that will ask you to which groups you would like to be added
  - In case you already are in the group it should be discarded and you need to be notified.
  - In case you are not in the group, it should be anounced in green color with black background that you are going to be part of the group.
  - Amount of groups provided to script should be at least 4.
  - **NOTE** Use Functions to Write this task

Please read in linux_fun - chapters: 26 ( yes again), 27-32
Please read in shell_scripting_with_bash : pages 38-72

### Lab 4

- Create new project at github/gitlab/bitbuket called `looping_new_gear` and upload all scripts into it.
  - Write a script that will receive($1) your name and will Print it in reverse.
- Write a script that will receive long sentance and will Print it in reverse.
- Write a script that will request 2 parameters(read), time and command.
  - the  script should notify that command should be executed at notified time (should work as clock alarm).
  - Please verify that you are recieving 2 paramenters
- Create a script that will check all possible files in your home folder, and if any of those won't belong to your User, it should be changed with "chown" command.
- Create a script that will receive file name: if it will be regular file, then Print out to its data, if it will be folder then Print out files in it.
- Write a script that will receive parameter of register User, and will Run until that User will login to system.
- Write a script that will receive parameter of a transportation(car, bus...), that paramenter should be of scenario in which type of transportation will Print how much it costs to buy it and how long one can hold to it. for example: car - costs 40000, nis can Use at least for 5 years.
- Write a script that will check what day is it today, some sort of cheer. for example: Monday - ohhh, Monday sucks.
- Create a scrit that will request your User name, will check if User exists, then will check that User ID is between 500 to 1000. if yes, then it should Print out Users Id and all possible data about him. if not, then it should notify the script User, wait 1.5 seconds and should close the terminal.

Please read: 
Linux_fun - chapters" 24,25,26(yes once again)
Shell_scrpting_with_bash - pages: till page 54
Pro_bash_programming - chapters:  2,3,6

Few links:
- http://www.freeos.com/guides/lsst/
- https://en.wikibooks.org/wiki/Bash_Shell_Scripting
- https://ryanstutorials.net/bash-scripting-tutorial/bash-script.php
- https://bash.cyberciti.biz/guide/Main_Page
- http://wiki.bash-hackers.org/
- https://arachnoid.com/linux/shell_programming.html
- https://lifelongstudent.io/pages/open-source/



### Lab 5

- Install several shells : csh, ksh, tcsh, fish, zsh
- Try to see if bash internals (control operators, shell variables, shell embedding) work the same
- Upload all scripts written to github/gitlab/bitbuket.
  - Read chapters 12,13,14,15
  - From exercises do: ex5,ex6,ex7,ex9,ex10,ex14
  - Do the Grub exercise
  - Start reading [bash-hackers](wiki.bash-hackers.org)
  - Please read about [aglie](https://en.wikipedia.org/wiki/Agile_software_development) methodology




### Lab 6

- Create repo at github/gitlab/bitbuket repo called 'pass to the dat to the array' in it save the next 3 labs: `Lab 6 ` `Lab 7` and `Lab 8`
- Write script that will verify all user password databaes :
  - create a variable that inlcudes all the files: /etc/passwd /etc/group /etc/shadow /etc/gshdow
  - loop through them
  - if they exists and are refular file, print it out else print the notification.


### Lab 7

- Write a scipt that gets 4 command and saves them into array. later loop though them and prints them out
  - use a loop to get 4 variable in to array
  - validate that you have 4 variables.
  - use another loop to go through elements of array.
  - print out notification message before runnung the command.


### Lab 8

- Create a script that will read from file line by line and will print the output in seperate fields:
  - create a variable that will store a file you'd like to print out: /etc/resolv.conf.
  - use OS variable IFS (read about it with man or help) to seperate the lines.
  - loop through process to print fields that will be seperated.
  - use input redirection to read from the file chosen at task 1.



### Lab 9 

- Create a repository called `space odyssey` and in it create scripts as follows:
-  Write a script with the name “servicemon”. the script should monitor the availability of a service, of which the name has been specified as a boot argument for the script. The script would run indefinitely, and if the service that it monitors stops for unknown reason it should behave as follows:
   -  Restart the service.
   -  Write a message to syslog.
   -  Send an email message to the root user.


### Lab 10

- Create a repository called `system tools` and create next scripts for it:
  - Write ascript called “message on the wall”, and in it, create code that will send a message to specific logged-in user. The message should be provided as positional parameter.Please pay attention to commands such as wall and write - they ARE to be used for this exercise
  - Write a script called the_whole_corner and with it monitor the biggest top 10 directories. ( user “du -S” command)


### Lab 11
- Create repository at github/gitlab/bitbuket called `Noobs library` and in it create next scripts:
  - Create a script that copies the contents of the log file /var/log/messages to /var/log/messages.old and deletes the contents  of the /var/log/messages file
  - Write a script named “totmp”. The script should copy all files from the names provided as arguments on the command line to the users home directory. If no files have been provided , the script should use read to ask for the file names and copy all the file names to provide in the answer to the user’s home directory
  -      Create a script that puts the result of the command : “date +%d-%m-%y” in variable. Use pattern matching on the variable to show 3 lines, displaying date, month and year in separated line as follows:
            The day is 17
            The month is 12
            The year is 17
  - Conjure a script that transforms the string : cn=bruce,dc=vaiolabs,dc=com in a way that the user name is extracted from the string. Also make sure that the result is written in a lowercase. Store the username in the variable NAME and at the end of the script, echo the value of this variable
  - Create a file called ldapuser and write in it next lines:
cn=bruce,dc=vaiolabs,dc=com
cn=wally,dc=vaiolabs,dc=com
cn=diana,dc=vaiolabs,dc=com
cn=john,dc=vaiolabs,dc=com
cn=zetana,dc=vaiolabs,dc=com
cn=clark,dc=vaiolabs,dc=com
The file simulates the extracted usernames of users from AD/LDAP. Write a script that extracts the username only from all the line and writes them into new file. Based on the new file, create new users on linux machine you are working on.
Note: never test the script on live machine, find a way to run the script that shows that all works without adding the users. After that add users.
  - Create a user helpdesk. Write a script that is started automatically when that user logs in. The menu script should never be terminated, unless the user logs out. From this menu: make following options available: 
    - 1. reset password
    - 2. show disk usage
    - 3. ping a host
    - 4. log out. 
  - You are welcome to add more options if you have them in mind.
  - Bonus: modify the script and related configuration so that the user can be sudo to set passwords for the other users as well.

---

[back to top](#labs)

---


##### `License`:
#####  **_This Material is developed by Alex M. Schapelle and [VAIOLabs inc](https://vaiolabs.com) and is distributed under [GPLv3](../Linux-Labs/LICENSE)_** 
