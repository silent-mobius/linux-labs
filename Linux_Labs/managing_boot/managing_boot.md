# Managing The Boot Process

## Lab 1
Your developers share a system that utilizes a desktop environment for the IDE tools. The other system administrator changed the environment so that it only boots into a command line shell, just before he went on vacation. You will need to correct the default boot environment.
- Check what is the current default target. Use the appropriate command to verify this.

<!-- systemctl get-default -->
- Change the default target to Graphical
  - You'll need administrator permissions to change the default target so that the computer boots into a graphical desktop.


---

## Lab 2

- On a server with graphical environment Installed: reboot
- From the GRUB boot prompt, start the text-only target.
- From text-only target, boot to the graphical target
- Ensure the your ssh service will be automatically started, andn that after a failure it will be restarted in 3 secunds.


---