# Managing Networking

## Lab 1

- Configure your hosts with persistent address Configuration, matching the subnet that is Used in your network. 
choose any ip address, as log as it allows you to connect to other networks.
- Configure ipv6 as well.

---

## Lab 2

Write a script that will :
- Print out all interfaces on your computer and ip of that interface.
- If the interface lacks ip address 
  - Create a Function that will guide you though the ip Configuration steps.
    - (Set ip - mandatory, Set netmask - mandatory, Set gateway- mandatory, Set dns - not mandatory)
- Write a Function that will check what type of linux disto you are Running.

---