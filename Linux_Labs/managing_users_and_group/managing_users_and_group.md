# Managing Users And Groups

## Lab 1

- Create 5 Users: Bruce, Barry, Diana, Courtney and Kara.
- Use input redirection, to Set password for all Users as 'P@ssw0rd'.
- Set their passwords to expire after 60 days.
- Create group jla and make Bruce,Diana,Kara and Barry part of it.
- Create group b.inc and make Diana, Bruce and Courtney part of it.
- Create another heros and add all the new Users to it.
- Ensure all Users get home directory at /home/.
- Set a limit on Users to Run up to 15 processes at the same time.

--- 

## Lab 2

- Create a project on github/gitlab/bitbuket called regExUp.
  - Create a script that will take a list of Users and will add them to your system.
  - The script should have a validation Function: it will check if User with that Username or nickname exists.(Use regex)
  - If User exists, script will stop and it will give grandioUse output that User or nickname already exists.
  - If User does not exists, script will continue and it will give grandioUse output that User or nickname is being Created.
- Create a script that will display a list of all the Users on your system who log in with the Bash shell as a default. (regex and /etc/skel/)
- Create a script that checks who was the last User that Created file /tmp and validate that User with /etc/passwd.(regex and /etc/passwd)

`Please chapters for next lesson: linuxsys.pdf -- 4,5,6,7,8,9,10,11,18,19,26,30`

--- 

## Lab 3

- Add the Users: tstart, bwayne, dprince to the Server
- Create the superhero Group
- Set wheel Group as the the tstark Account's Primary Group
- Add superhero as a Supplementary Group on All Three Users
- Lock the dprince Account

---

## Lab 4

- Create two new users.
- Verify the /etc/sudoers file and test access.
- Set up the web administrator.

---

## Lab 5

- Enable SSH to Connect Without a Password from the dev User on server1 to the dev User on server2
- Copy All tar Files rom /home/dev/ on server1 to /home/dev/ on server2, and Extract Them Making Sure the Output is Redirected to /home/dev/tar-output.log
- Set the Umask So That New Files Are Only Readable and Writeable by the Owner
- Verify the /home/dev/deploy.sh Script Is Executable and Run It

---

## Lab 6

#### NOTE
verify that server is up first.

- Set the Server up to Authenticate Using ldap.example.com
- Get the UID of ldapuser3

---

# links
https://www.regexone.com/ - regex tutorial. https://regexper.com - try testing various regex via this link.