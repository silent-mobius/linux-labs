# Working With Text Files

## Lab 1

- Display the 5th line of passwd file.
- Do the same with different file.
- Show first column of `ps aux`.
- Catch all the files in /etc/ that end with .Conf.
- Get name of all files that contain at most 4 letters.
- Filter out from search output the name User but, validate that User root is not in the list.


---