# VaioLabs Practical Labs in Infrastructure, Code and Automation

- [Code]
  - [Bash]()
  - [C]()
  - [Groovy]()
  - [Markdown]()
  - [Python](./Python_Labs/README.md)
  - [Ruby]()
  - [Yaml]()
- [DevOps]
  - [Automation]()
  - [Infrastructire As A Code]()
    - [Cloud]()
    - [On Prem]()
  - [Continues Integration]()
  - [Continues Deployment]()
  - [Containers]()
  - [Container Management]()
  - [Continues Monitoring]()
- [Linux Basics]
  - [install](./Linux_Labs/linux_install/linux_install.md)
  - [essentials](./Linux_Labs/using_linux_essential/using_linux_essential.md)
  - [files in linux](./Linux_Labs/using_file_management_tools/using_file_management_tools.md)
  - [files in linux ... again](./Linux_Labs/working_with_text_files/working_with_text_files.md)
  - [detecting the hardware](./Linux_Labs/hardware_check_and_monitor/hardware_check_and_monitor.md)
  - [connecting to server](./Linux_Labs/connecting_to_server/connecting_to_server.md)
  - [configuring firewall](./Linux_Labs/configuring_firewall/configuring_firewall.md)
  - [managing time](./Linux_Labs/managing_time/manging_time.md)
  - [managing processes](./Linux_Labs/managing_processes/managing_processes.md)
  - [managing software](./Linux_Labs/managing_software/managing_software.md)
  - [scheduling tasks](./Linux_Labs/scheduling_tasks/scheduling_tasks.md)
  - [configuring logging](./Linux_Labs/configuring_loggin/configuring_loggin.md)
  - [managing the linux kernel](./Linux_Labs/kernel/kernel.md)
  - [managing the boot process](./Linux_Labs/managing_boot/managing_boot.md)
  - [managing mandatory access control](./Linux_Labs/managing_security/managing_security.md)
  - [managing partitions and file systems](./Linux_Labs/managing_partitions_and_fs/managing_partitions_and_fs.md)
  - [managing logical volumes](./Linux_Labs/managing_lvm/managing_lvm.md)
  - [managing software RAID](./Linux_Labs/managing_raid/managing_raid.md)
  - [managing web services](./Linux_Labs/managing_web/managing_web.md)
  - [managing ftp services](./Linux_Labs/manging_ftp/managing_ftp.md)





